terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region     = "us-east-1"
  access_key = "AKIA2GG4NCWT5XJJ6UOM"
  secret_key = "F+H96rVa39V/BL8kSsFe7LR4QTW0SSJZ1lbi1coJ"
}

resource "aws_instance" "example" {
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name      = "terraform"
  iam_instance_profile = "PuppetEC2Role"

  tags = {
    Name = "terraformPuppetintegration"
  }

  connection {
    type        = "ssh"
    user        = "shivanjali"  # Replace with the appropriate user for your AMI (e.g., ec2-user, ubuntu, etc.)
    private_key = file("D:/BE/worldline/task4/assignment/terraform.pem")  # Replace with the path to your private key
    host        = aws_instance.example.public_ip  # Corrected to use aws_instance.example.public_ip
  
   agent       = true
      host_key    = true
  }

  provisioner "remote-exec" {
    inline = [
      
      "sudo apt-get update",
      "wget https://apt.puppetlabs.com/puppet8-release-jammy.deb",
      "sudo dpkg -i puppet8-release-jammy.deb",
      "sudo apt update -y",
      "sudo apt install -y puppetserver",
      "sudo sed -i -e 's/-Xms2g -Xmx2g / -Xms250m -Xmx250m /g' /etc/default/puppetserver",
      "sudo systemctl start puppetserver",

      "puppet apply --modulepath=D:/BE/worldline/task4/assignment/puppetterraformintegration/puppet D:/BE/worldline/task4/assignment/puppetterraformintegration/puppet/site.pp",  # Trigger Puppet run
   
    ]
  }
     
}
